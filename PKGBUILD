# Maintainer: kpcyrd <kpcyrd[at]archlinux[dot]org>
# Maintainer: Orhun Parmaksız <orhun@archlinux.org>
# Contributor: Jian Zeng <anonymousknight96+aur AT gmail.com>
# Contributor: Alexandre Bury <alexandre.bury+aur AT gmail.com>
# Contributor: Vlad M. <vlad@archlinux.net>

pkgname=cargo-outdated
pkgver=0.16.0
pkgrel=2
pkgdesc="A cargo subcommand for displaying when Rust dependencies are out of date"
url="https://github.com/kbknapp/cargo-outdated"
arch=('x86_64')
license=('MIT')
depends=(
  'cargo'
  'gcc-libs'
  'glibc'
  'libcurl.so'
  'libssh2.so'
  'libssl.so'
  'libz.so'
)
source=("${pkgname}-${pkgver}.tar.gz::https://crates.io/api/v1/crates/${pkgname}/${pkgver}/download"
        rust-edition2024.patch)
sha256sums=('965d39dfcc7afd39a0f2b01e282525fc2211f6e8acc85f1ee27f704420930678'
            '8cc3fd2064ccd27aa3fbfcd5077e0feb755f9d6cc7e1370917fd3b88f6a58656')
b2sums=('5691d93c204f7437ce0e3e035ed916aa476f13174784d90f27eddcadc324bd96f75bc9d4a9221471e107881e92117d84eb525fe37c958e0d16d02961c3e680f1'
        'e722fc4da5bffa8121716752386811ffccb738e729285502c528686def48c54a02da922cf5671ce46544191df6fd53d72c119212542003a06a0facbeb769c439')
options=('!lto')

prepare() {
  cd "${pkgname}-${pkgver}"
  patch -Np1 -i ../rust-edition2024.patch
  cargo fetch --locked --target "$(rustc -vV | sed -n 's/host: //p')"
}

build() {
  cd "${pkgname}-${pkgver}"
  export LIBSSH2_SYS_USE_PKG_CONFIG=1
  cargo build --frozen --release
}

package() {
  cd "${pkgname}-${pkgver}"
  install -Dm755 "target/release/${pkgname}" "${pkgdir}/usr/bin/${pkgname}"
  install -Dm644 LICENSE-MIT -t "${pkgdir}/usr/share/licenses/${pkgname}"
}

# vim: ts=2 sw=2 et:
